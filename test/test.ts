import { expect, tap } from '@pushrocks/tapbundle';
import * as kubernetes from '../ts/index';

let testClient: kubernetes.KubeClient;

if (process.env.CI) {
  process.exit(0);
}

tap.test('first test', async () => {
  testClient = new kubernetes.KubeClient();
});

tap.test('should init the client', async () => {
  await testClient.init();
});

tap.test('should be able to set a certificate', async () => {
  await testClient.setSslSecret('central.eu', {
    created: 123456,
    csr: '',
    domainName: '*.central.eu',
    id: 'hu7e6rw',
    privateKey: 'wowza',
    publicKey: 'hellothere'
  });
});

tap.test('should get a secret', async () => {
  const result = await testClient.getSslSecret('central.eu');
  console.log(result);
});

tap.start();
