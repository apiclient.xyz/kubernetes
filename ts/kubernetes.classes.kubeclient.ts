import * as plugins from './kubernetes.plugins';
import * as paths from './kubernetes.paths';

export class KubeClient {
  public client: plugins.kubectl.ApiRoot;

  constructor() {}

  public async init() {
    const { KubeConfig } = require('kubernetes-client');
    const kubeconfig = new KubeConfig();
    kubeconfig.loadFromFile(paths.defaultKubeConfigPath);
    const Request = require('kubernetes-client/backends/request');

    const backend = new Request({ kubeconfig });
    this.client = new plugins.kubectl.Client1_13({ backend, version: '1.13' });
  }

  public async setRegistry() {}

  /**
   * sets an ssl secret
   * @param domainNameArg
   * @param sslCertificateArg
   */
  public async setSslSecret(
    domainNameArg: string,
    sslCertificateArg: plugins.tsclass.network.ICert
  ) {
    this.client.api.v1.namespace('default').secret.post({
      body: {
        apiVersion: 'v1',
        kind: 'Secret',
        metadata: {
          name: `lossless-ssl-${domainNameArg}`
        },
        type: 'Opaque',
        data: {
          'tls.crt': plugins.smartstring.base64.encode(sslCertificateArg.publicKey),
          'tls.key': plugins.smartstring.base64.encode(sslCertificateArg.privateKey)
        }
      }
    });
  }

  public async getSslSecret(domainNameArg: string) {
    this.client.api.v1
      .namespace('default')
      .secrets(domainNameArg)
      .get();
  }

  /**
   * removes an ssl secret
   * @param domainName
   */
  public async removeSslSecret(domainName: string) {
    this.client.api.v1
      .namespace('default')
      .secrets(domainName)
      .delete();
  }

  public async setService() {}

  public async deleteService() {}

  public async installIngress() {}
}
